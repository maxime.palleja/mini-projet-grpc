const express = require('express');
const bodyParser = require('body-parser');
const grpc = require('@grpc/grpc-js');
const protoLoader = require('@grpc/proto-loader');
const path = require('path');

const packageDefinition = protoLoader.loadSync('calculator.proto', {});
const calculatorProto = grpc.loadPackageDefinition(packageDefinition).calculator;

const client = new calculatorProto.Calculator('localhost:50051', grpc.credentials.createInsecure());

const app = express();
const port = 3000;

app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));

app.post('/calculate', (req, res) => {
    const { num1, num2, operation } = req.body;
    client[operation]({ num1, num2 }, (error, response) => {
        if (error) {
            return res.status(500).send(error);
        }
        res.json(response);
    });
});

app.listen(port, () => {
    console.log(`Express server running at http://localhost:${port}`);
});
