const grpc = require('@grpc/grpc-js');
const protoLoader = require('@grpc/proto-loader');
const packageDefinition = protoLoader.loadSync('calculator.proto', {});
const calculatorProto = grpc.loadPackageDefinition(packageDefinition).calculator;

const server = new grpc.Server();

server.addService(calculatorProto.Calculator.service, {
  Add: (call, callback) => {
    const result = call.request.num1 + call.request.num2;
    callback(null, { result });
  },
  Subtract: (call, callback) => {
    const result = call.request.num1 - call.request.num2;
    callback(null, { result });
  },
  Multiply: (call, callback) => {
    const result = call.request.num1 * call.request.num2;
    callback(null, { result });
  },
  Divide: (call, callback) => {
    if (call.request.num2 === 0) {
      return callback({
        code: grpc.status.INVALID_ARGUMENT,
        message: 'Division by zero is not allowed'
      });
    }
    const result = call.request.num1 / call.request.num2;
    callback(null, { result });
  }
});

server.bindAsync('0.0.0.0:50051', grpc.ServerCredentials.createInsecure(), () => {
  console.log('gRPC server running at http://0.0.0.0:50051');
  server.start();
});
